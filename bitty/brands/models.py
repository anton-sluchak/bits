from banks.models import Banks
from django.db import models


class BittyBrand(models.Model):
    name = models.CharField(max_length=100)

    # For now it will be a static value for each brand.
    # Later we might add an intermediate model to define personal rewards
    std_reward_permille = models.IntegerField()


class BankBrand(models.Model):
    bank = models.ForeignKey(Banks, on_delete=models.RESTRICT)
    bitty_brand = models.ForeignKey(BittyBrand, on_delete=models.RESTRICT)
    bank_brand_id = models.CharField(max_length=100)  # Char as it can be in a form of a hash

    def __str__(self):
        return self.bitty_brand.name
