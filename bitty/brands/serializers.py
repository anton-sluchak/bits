from rest_framework import serializers

from .models import BittyBrand


class BrandSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = BittyBrand
        fields = "__all__"
