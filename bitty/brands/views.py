from rest_framework import viewsets

from .models import BittyBrand
from .serializers import BrandSerializer


class BittyBrandViewSet(viewsets.ModelViewSet):
    queryset = BittyBrand.objects.all()
    serializer_class = BrandSerializer
    permission_classes = []
