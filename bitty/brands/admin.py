from django.contrib import admin

from .models import BankBrand, BittyBrand

admin.site.register(BittyBrand)
admin.site.register(BankBrand)
