from .celery import app
from celery.utils.log import get_task_logger

logger = get_task_logger(__name__)


@app.task
def sync_transactions():
    logger.info("Transactions synced between banks app")


@app.task
def provide_rewards():
    logger.info("rewards given based on transactions")
    logger.info("Calculate  given based on transactions")
