from django.contrib import admin

from .models import Stock, StockReward

admin.site.register(Stock)
admin.site.register(StockReward)
