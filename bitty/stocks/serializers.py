from rest_framework import serializers
from stocks.models import Stock, StockReward


class StockSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Stock
        fields = "__all__"


class StockRewardSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = StockReward
        fields = "__all__"
