from rest_framework import viewsets

from .models import Stock, StockReward
from .serializers import StockRewardSerializer, StockSerializer


class StockViewSet(viewsets.ModelViewSet):
    queryset = Stock.objects.all()
    serializer_class = StockSerializer
    permission_classes = []


class StockRewardViewSet(viewsets.ModelViewSet):
    queryset = StockReward.objects.all()
    serializer_class = StockRewardSerializer
    permission_classes = []
