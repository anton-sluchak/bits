from brands.models import BittyBrand
from django.db import models
from transactions.models import Transaction
from users.models import BittyUser


class Stock(models.Model):
    bitty_brand = models.ForeignKey(BittyBrand, on_delete=models.RESTRICT)

    # For test task it will be static, although in real world there will be another service,
    # That will save prices per day or even at a given transaction time.
    share_price_cents = models.IntegerField()


class StockReward(models.Model):
    datetime = models.DateTimeField()
    user = models.ForeignKey(BittyUser, on_delete=models.RESTRICT)
    stock = models.ForeignKey(Stock, on_delete=models.RESTRICT)

    transaction = models.ForeignKey(Transaction, on_delete=models.RESTRICT)

    # Not to use floating point there would be a minimal share particle that this will be used in this field
    shares_reward = models.IntegerField()

    # For easier calculations will be saved here
    monetary_value = models.IntegerField()
