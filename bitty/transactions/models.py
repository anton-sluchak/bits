from brands.models import BittyBrand
from django.db import models
from users.models import BittyUser


class Transaction(models.Model):
    class TransactionType(models.TextChoices):
        DEBIT = ('debit', 'Debit')
        CREDIT = ('credit', 'Credit')

    datetime = models.DateTimeField()
    user = models.ForeignKey(BittyUser, on_delete=models.RESTRICT)
    brand = models.ForeignKey(BittyBrand, on_delete=models.RESTRICT)
    amount_cents = models.IntegerField()
    type = models.CharField(
        max_length=6,
        choices=TransactionType.choices
    )
