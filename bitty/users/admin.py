from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from users.models import BittyUser

admin.site.register(BittyUser, UserAdmin)