from brands.models import BittyBrand
from django.contrib.auth.models import AbstractUser
from django.db import models


class BittyUser(AbstractUser):
    favourite_brands = models.ManyToManyField(BittyBrand)
