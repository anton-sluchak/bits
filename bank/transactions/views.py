from rest_framework import viewsets

from .serializers import TransactionsSerializer, BrandsSerializer
from .models import Transaction, Brand


class TransactionViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Transaction.objects.all()
    serializer_class = TransactionsSerializer


class BrandViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Brand.objects.all()
    serializer_class = BrandsSerializer
