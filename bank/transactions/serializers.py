from .models import Transaction, Brand
from rest_framework import serializers


class TransactionsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Transaction
        fields = '__all__'


class BrandsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Brand
        fields =  '__all__'
