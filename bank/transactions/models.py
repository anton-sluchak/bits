from django.db import models
from users.models import BankUser


class Brand(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Transaction(models.Model):
    class TransactionType(models.TextChoices):
        DEBIT = ('debit', 'Debit')
        CREDIT = ('credit', 'Credit')

    datetime = models.DateTimeField()
    user = models.ForeignKey(BankUser, on_delete=models.RESTRICT)
    bank_brand = models.ForeignKey(Brand, on_delete=models.RESTRICT)
    amount_cents = models.IntegerField()
    type = models.CharField(
        max_length=6,
        choices=TransactionType.choices
    )
