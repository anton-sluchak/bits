from django.contrib import admin
from .models import Brand, Transaction

admin.site.register(Brand)
admin.site.register(Transaction)

