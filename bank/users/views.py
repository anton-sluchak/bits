from rest_framework.decorators import action
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response

from transactions.models import Transaction
from transactions.serializers import TransactionsSerializer
from rest_framework import viewsets

from .models import BankUser
from .serializers import UserSerializer


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = BankUser.objects.all().order_by('email')
    serializer_class = UserSerializer

    @action(methods=['get'], detail=True)
    def transactions(self, request, pk=None):
        queryset = Transaction.objects.filter(user=pk)
        transactions = get_object_or_404(queryset, pk=pk)
        serializer = TransactionsSerializer(transactions, context={'request': request})
        return Response(serializer.data)
