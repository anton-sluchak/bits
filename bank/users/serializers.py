from .models import BankUser
from rest_framework import serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = BankUser
        fields = '__all__'
