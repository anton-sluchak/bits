from django.db import models
from django.contrib.auth.models import AbstractBaseUser


class BankUser(AbstractBaseUser):
    email = models.EmailField(unique=True)
    USERNAME_FIELD = 'email'

    def __str__(self):
        return self.email
