#!/bin/sh
# wait-for-postgres.sh

set -e

host="$1"
shift

until PGPASSWORD=$POSTGRES_PASSWORD psql -h "$host" -U "postgres" -c '\q'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 3
done

>&2 echo "Postgres is up - executing command"

echo "Run migrations"
python "manage.py" "migrate"

echo "Run Django"
exec "python" "manage.py" "runserver" "0.0.0.0:8000"